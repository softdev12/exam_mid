/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nattiya.exam_mid;

import java.io.Serializable;

/**
 *
 * @author DELL
 */
public class Product implements Serializable{

    private String id;
    private String pdName;
    private String pdBrand;
    private double pdPrice;
    private int pdAmount;

    public Product(String id, String pdName, String pdBrand, double pdPrice, int pdAmount) {
        this.id = id;
        this.pdName = pdName;
        this.pdBrand = pdBrand;
        this.pdPrice = pdPrice;
        this.pdAmount = pdAmount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPdName() {
        return pdName;
    }

    public void setPdName(String pdName) {
        this.pdName = pdName;
    }

    public String getPdBrand() {
        return pdBrand;
    }

    public void setPdBrand(String pdBrand) {
        this.pdBrand = pdBrand;
    }

    public double getPdPrice() {
        return pdPrice;
    }

    public void setPdPrice(double pdPrice) {
        this.pdPrice = pdPrice;
    }

    public int getPdAmount() {
        return pdAmount;
    }

    public void setPdAmount(int pdAmount) {
        this.pdAmount = pdAmount;
    }

    @Override
    public String toString() {
        return "ID = " + id + 
               "   Name = " + pdName + 
               "        Brand = " + pdBrand + 
               "        Price = " + pdPrice + 
               "        Amount = " + pdAmount;
    }

}
